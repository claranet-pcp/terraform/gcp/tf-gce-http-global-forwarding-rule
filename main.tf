resource "google_compute_global_forwarding_rule" "global_forwarder" {
  name       = "global-forwarder-${var.service}"
  ip_address = "${var.ip_address}"
  target     = "${google_compute_target_http_proxy.proxy.self_link}"
  port_range = 80
}

resource "google_compute_target_http_proxy" "proxy" {
  name    = "http-proxy-${var.service}"
  url_map = "${google_compute_url_map.url_map.self_link}"
}

resource "google_compute_url_map" "url_map" {
  name            = "url-map-${var.service}"
  default_service = "${var.default_backend}"
}
